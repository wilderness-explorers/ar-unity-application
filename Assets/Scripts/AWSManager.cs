﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Amazon;
using Amazon.S3;
using Amazon.CognitoIdentity;
using Amazon.S3.Model;
using UnityEngine.Networking;
using UnityEngine.UI;

public class AWSManager : MonoBehaviour
{
    // To unload the current bundle when the character is changed.
    AssetBundle currentBundle;

    // To hold the character thats being instantiated. So it can be destroyed when needed.
    GameObject characterHolder;
    Transform refCharacterHolder;
    public Text logText;
    private float loadingTime;
    private bool once;

    // Singleton
    private static AWSManager instance;
    public static AWSManager Instance
    {
        get
        {
            if(instance == null)
            {
                Debug.LogWarning("AWS Manager is null");
            }
            return instance;
        }
    }

    public string S3Region = RegionEndpoint.APSouth1.SystemName;
    private RegionEndpoint _S3Region
    {
        get
        {
            return RegionEndpoint.GetBySystemName(S3Region);
        }
    }

    // Property that will return the connection to Amazon.
    private AmazonS3Client _s3Client;
    public AmazonS3Client S3Client
    {
        get
        {
            if(_s3Client == null)
            {
                // Client config - identity pool.
                _s3Client = new AmazonS3Client(new CognitoAWSCredentials("ap-south-1:e3a615c2-dd5e-4d84-a380-f8c591b8b35f", RegionEndpoint.APSouth1), _S3Region);
            }

            return _s3Client;
        }
    }


    private void Awake()
    {
        instance = this;
        characterHolder = null;
        currentBundle = null;
        loadingTime = 0.0f;
        once = true;

        // Setup Amazon S3 client.
        // The object that is going to communicate with AWS server, will be attached to this object.
        UnityInitializer.AttachToGameObject(this.gameObject);

        // Configure http client.
        // For all http calls, Unity web request will be used.
        AWSConfigs.HttpClient = AWSConfigs.HttpClientOption.UnityWebRequest;

        // Grab information from the bucket.
        var request = new ListObjectsRequest()
        {
            BucketName = "assetbundlesarapp"
        };

        // Fetch the data and passes in the request. Response object obtained back from AWS.
        S3Client.ListObjectsAsync(request, (responseObject) =>
        {
            // Filter through the response and log the data.
            if(responseObject.Exception == null)
            {
                responseObject.Response.S3Objects.ForEach((obj) =>
                {
                    // obj.key gives the file name.
                    Debug.Log("Obj: " + obj.Key);
                });
            }
            else
            {
                Debug.LogWarning("Response exception: " + responseObject.Exception);
                logText.text += "\nResponse exception: " + responseObject.Exception.ToString();
            }
        });
    }

    public void DownloadBundle(string animationName)
    {
        // To get a reference of position and transform of the character spawned in AR. First time only.
        if(once)
        {
            refCharacterHolder = GameObject.FindGameObjectWithTag("Player").transform;
            refCharacterHolder.gameObject.SetActive(false);

            once = false;
        }
        
        loadingTime = Time.time;

        // If a character has been already loaded.
        if (characterHolder != null)
        {
            // Before loading another character - get rid of the current bundle.
            currentBundle.Unload(false);
            Destroy(characterHolder);
        }

        logText.text += "\nDownloading new animation...";
        StartCoroutine(BundleRoutine(animationName));
    }

    IEnumerator BundleRoutine(string name)
    {
        var unityWebRequest = UnityWebRequestAssetBundle.GetAssetBundle("https://assetbundlesarapp.s3.ap-south-1.amazonaws.com/characteranimation-Android");
        Debug.Log("Web request in process...");
        logText.text += "\nWeb request in process...";

        // Depends on the internet connection.
        yield return unityWebRequest.SendWebRequest();
        Debug.Log("Web request completed.");
        logText.text += "\nWeb request completed.";

        currentBundle = DownloadHandlerAssetBundle.GetContent(unityWebRequest);
        var loadAsset = currentBundle.LoadAssetAsync(name);
        logText.text += "\nLoading in progress: " + name.ToString();
        //var loadAsset = currentBundle.LoadAssetAsync<GameObject>(currentBundle.GetAllAssetNames()[0]);
        yield return loadAsset;
        logText.text += "\nLoading completed. Elapsed time = " + (Time.time - loadingTime).ToString();

        // Assigning the character.
        characterHolder = (GameObject)Instantiate(loadAsset.asset, refCharacterHolder.position, refCharacterHolder.rotation);
        characterHolder.transform.localScale = refCharacterHolder.transform.localScale;
    }
}