﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.UI;

public class ARPlacement : MonoBehaviour
{
    public GameObject spawnObject;
    private GameObject spawnedObject;
    public GameObject placementIndicator;

    private Pose placementPose;
    private ARRaycastManager aRRaycastManager;
    private bool isValidPose = false;

    public GameObject mainCanvas;
    public Text logText;
    private float initialDistance;
    private Vector3 initialScale;

    private void OnEnable()
    {
        logText.text = "Scan ground area. Tap to place the object.";
        mainCanvas.SetActive(false);
    }

    void Start()
    {
        aRRaycastManager = FindObjectOfType<ARRaycastManager>();
    }

    // Used to update placement indicator, placement pose and to spawn.
    void Update()
    {
        if(spawnedObject == null && isValidPose && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            PlaceARObject();

            // After placing the model on the surface, 5 buttons will appear.
            mainCanvas.SetActive(true);
        }

        // scale using pinch involves two touches
        // Count both the touches, store and measure the distance between pinch 
        // Scale gameobject depending on the pinch distance
        // we also need to ignore if the pinch distance is small.

        if (Input.touchCount == 2)
        {
            var touchZero = Input.GetTouch(0);
            var touchOne = Input.GetTouch(1);

            // if any one of touchzero or touchOne is cancelled or maybe ended then do nothing
            if (touchZero.phase == TouchPhase.Ended || touchZero.phase == TouchPhase.Canceled ||
                touchOne.phase == TouchPhase.Ended || touchOne.phase == TouchPhase.Canceled)
            {
                return;
            }

            if (touchZero.phase == TouchPhase.Began || touchOne.phase == TouchPhase.Began)
            {
                initialDistance = Vector2.Distance(touchZero.position, touchOne.position);
                initialScale = spawnedObject.transform.localScale;
                Debug.Log("Initial Distance: " + initialDistance + "GameObject Name: " + spawnObject.name); // Just to check in console
            }
            else // if touch is moved
            {
                var currentDistance = Vector2.Distance(touchZero.position, touchOne.position);

                //if accidentally touched or pinch movement is very very small
                if (Mathf.Approximately(initialDistance, 0))
                {
                    // do nothing if it can be ignored where inital distance is very close to zero
                    return;
                }

                var factor = currentDistance / initialDistance;
                // scale multiplied by the factor we calculated
                spawnedObject.transform.localScale = initialScale * factor;
            }
        }


        UpdatePlacementIndicator();
        UpdatePlacementPose();
    }

    private void UpdatePlacementIndicator()
    {
        if(spawnedObject == null && isValidPose)
        {
            placementIndicator.SetActive(true);
            placementIndicator.transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);
        }
        else
        {
            placementIndicator.SetActive(false);
        }
    }

    private void UpdatePlacementPose()
    {
        var midPoint = Camera.current.ViewportToScreenPoint(new Vector2(0.5f, 0.5f));
        var hits = new List<ARRaycastHit>();

        aRRaycastManager.Raycast(midPoint, hits, TrackableType.Planes);

        if(hits.Count > 0)
        {
            isValidPose = true;
        }
        else
        {
            isValidPose = false;
        }

        if(isValidPose)
        {
            placementPose = hits[0].pose;
        }
    }

    private void PlaceARObject()
    {
        spawnedObject = Instantiate(spawnObject, placementPose.position, placementPose.rotation);
        logText.text += "\nAR object placed. Pinch to scale. ";
    }
}