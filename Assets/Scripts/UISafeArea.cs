﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISafeArea : MonoBehaviour
{
    RectTransform rectTransform;
    Rect safeRegion;

    Vector2 minAnchor;
    Vector2 maxAnchor;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        safeRegion = Screen.safeArea;

        minAnchor = safeRegion.position;
        maxAnchor = minAnchor + safeRegion.size;

        //Assigning width and height of the screen to the x and y positions of min and max anchor.
        minAnchor.x /= Screen.width;
        minAnchor.y /= Screen.height;

        maxAnchor.x /= Screen.width;
        maxAnchor.y /= Screen.height;

        rectTransform.anchorMin = minAnchor;
        rectTransform.anchorMax = maxAnchor;
    }
}
