﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationController : MonoBehaviour
{
    Animator animator;
    bool isAnimationPlaying = false;
    public Button[] buttons;

    public void PlayAnimation()
    {
        if (!isAnimationPlaying)
        {
            animator = FindObjectOfType<Animator>().GetComponent<Animator>();
            AnimatorStateInfo animationInfo = animator.GetCurrentAnimatorStateInfo(0);
            AnimationClip[] clips = animator.runtimeAnimatorController.animationClips;

            float elapsedTime = animationInfo.normalizedTime;
            float clipLength = 0.0f;

            foreach (AnimationClip clip in clips)
            {
                clipLength = clip.length;
            }

            Invoke("ResetButtons", clipLength);
            Debug.Log(clipLength.ToString());

            // If normalizedTime is 0 to 1 indicates animation is playing, if greater than 1 means finished.
            if ((elapsedTime <= 0.0f || elapsedTime >= 1.0f))
            {
                isAnimationPlaying = true;

                // Deactivate buttons to change character or play animation while current animation is playing.
                foreach (Button button in buttons)
                {
                    button.interactable = false;
                }

                animator.SetTrigger("Play");
            }
        }
    }

    // Reset buttons back to active state after animation stops.
    public void ResetButtons()
    {
        isAnimationPlaying = false;

        foreach (Button button in buttons)
        {
            button.interactable = true;
        }
    }
}